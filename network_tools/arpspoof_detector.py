"""
Author: Mthandazo Ndhlovu
Project: arpspoof_detector.py
"""
from scapy.all import sniff
IP_MAC_MAP = {}

def     process_packet(packet) -> str:
    src_ip = packet['ARP'].psrc
    src_mac = packet['Ether'].src
    if src_map in IP_MAC_MAP:
        if IP_MAC_MAP[src_map] != src_ip:
            try:
                prev_ip = IP_MAC_MAP[src_mac]
            except:
                prev_ip = "unknown"
            message = ("\n Possible ARP Attack detected \n" + 
                       "It is possible that the machine with IP addresses \n" + 
                       str(prev_id) + " is pretending to be " + str(src_ip) + "\n")

            return message
    else:
        IP_MAC_MAP[src_mac] = src_ip


sniff(count=0, filter="arp", store=0, prn=process_packet)
